import { Address, BigInt, ethereum } from "@graphprotocol/graph-ts";
import { SAO, Transfer } from "../../generated/SAO/SAO";
import { TransferEntity, BlockEntity } from "../../generated/schema";

const TOKEN_ADDRESS = "0x029006ebECB873E60F699CB48Ba152cEb35f1853";

export function onTransfer(event: Transfer): void {
    let transfer = TransferEntity.load(event.transaction.hash.toHexString());

    if (transfer == null) {
        let transfer = new TransferEntity(event.transaction.hash.toHexString());
        transfer.transferAmount = event.params.value;

        let tokenPrototype = SAO.bind(Address.fromString(TOKEN_ADDRESS));
        let resp = tokenPrototype.try_balanceOf(event.params.from);
        
        if (resp.reverted){
            transfer.address1BalanceOf = BigInt.zero();
            transfer.isReverted1 = true;
        }else{
            transfer.address1BalanceOf = resp.value;
            transfer.isReverted1 = false;
        }

        resp = tokenPrototype.try_balanceOf(event.params.to);


        if (resp.reverted){
            transfer.address2BalanceOf = BigInt.zero();
            transfer.isReverted2 = true;
        }else{
            transfer.address2BalanceOf = resp.value;
            transfer.isReverted2 = false;
        }
        transfer.save();
    }
}

export function handl(block: ethereum.Block): void {
    let bl = BlockEntity.load(block.hash.toHexString());

    if (bl == null) {
        bl = new BlockEntity(block.hash.toHexString());
        bl.timestamp = block.timestamp;
        bl.hash = block.hash;
    }
    bl.save();
}